# OSM FaSpaceFixer
A script for fixing spacing issue in name tag on OpenStreetMap Iran.

Example: 
name=بهار۱۰ >>> name=بهار ۱۰
name=بهار۱۰شرقی >>> name=بهار ۱۰ شرقی    , etc... 
<div dir="rtl">
این اسکریپت برای اصلاح نقشه OpenStreetMap در محدوده ایران تهیه و توسعه داده شده است و موارد مشکلات مطرح شده در لینک زیر (انجمن) را برطرف میکند:

https://forum.openstreetmap.org/viewtopic.php?pid=760505#p760505

به طور خلاصه :

اصلاح و اضافه کردن فاصله گذاری لازم قبل و بعد از اعداد در تگ نام.

توجه: این اسکریپت فقط باید با اکانت اختصاصی اسکریپت به نام زیر اجرا شود

https://www.openstreetmap.org/user/FaSpaceFixerBot

لطفا از اجرا و آپلود کردن تغییرات بدون هماهنگی بپرهیزید.

لینک صفحه ویکی : https://wiki.openstreetmap.org/wiki/Automated_edits/FaSpaceFixer

### Log
لیست آبجکت‌های تغییر یافته رو می‌توانید در [این](/logs) آدرس ببینید.
</div>
