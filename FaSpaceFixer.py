#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Converting English digits [1234567890] and also Arabic digits [٤٥٦] into their equivalents in Perisan encoding [۴۵۶].

import re,fileinput,sys,os,datetime
import xml.etree.ElementTree as ET
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/modules")
import pytz


os.system('cls')

logfile = open(os.path.dirname(os.path.realpath(__file__))+"/log.txt", 'ab')
now = datetime.datetime.now(pytz.timezone('Asia/Tehran'))
currentdatetime = now.strftime("%Y-%m-%d_%H-%M-%S")
logchangesfile = open(os.path.dirname(os.path.realpath(__file__))+"/logs/Changes_"+currentdatetime+".html", 'wb')
logchangesfile.write(b"""<!DOCTYPE html>
<html dir="rtl" lang="fa">
<head>
<title>OSM FaSpaceFixer Changes Result</title>
<meta charset="utf-8">
<style>
#changestbl{font-family:"Trebuchet MS",Arial,Helvetica,sans-serif;border-collapse:collapse;width:100%}#changestbl td,#changestbl th{border:1px solid #ddd;padding:8px;white-space:pre;}#changestbl tr:nth-child(even){background-color:#f2f2f2}#changestbl tr:hover{background-color:#ddd}#changestbl th{padding-top:12px;padding-bottom:12px;text-align:center;background-color:#4caf50;color:#fff}#changestbl td{text-align:center}
</style>
<script>function closeOnLoad(n){var o=window.open(n,"connectWindow","width=600,height=400,scrollbars=yes");return setTimeout(function(){o.close()},1e3),!1}</script>
</head>
<body>
<div align="center" dir="auto" style="white-space:pre;line-height: 11pt;"><h3>{{info}}</h3></div>
<table id="changestbl">
<tr><th>#</th><th>ID</th><th>Old Name</th><th>New Name</th><th>Open In Editor</th></tr>
""")
info = "Changeset: 		| Date: "+now.strftime("%Y/%m/%d %H:%M:%S")+" IRST"
def log(text):
	print(text)
	text = str(text)
	text = now.strftime("%Y-%m-%d %H:%M:%S")+":   "+text+"\n"
	text = text.encode('utf8')
	logfile.write(text)

def logchanges(n,id,oldname,newname):
	n=str(n)
	josm=id.replace("node/","n").replace("way/","w").replace("relation/","r").encode('utf-8')
	iD=id.replace("node/","node=").replace("way/","way=").replace("relation/","relation=").encode('utf-8')
	logchangesfile.write(b"""<tr><td>%s</td><td><a href='https://www.openstreetmap.org/%s/history'>%s</a></td><td>%s</td><td>%s</td><td><a href='https://openstreetmap.org/edit?editor=id&zoom=17&%s' target='_blank'>iD</a> | <a href='#%s' onclick='javascript:closeOnLoad("http://127.0.0.1:8111/load_object?new_layer=true&objects=%s");'>JOSM</a></td></tr> """% (n.encode('utf-8'),id.encode('utf-8'),id.encode('utf-8'),oldname.encode('utf-8'),newname.encode('utf-8'),iD,josm,josm)+b"\n")
	

def dofixes(name):
	if re.match(r"[\u0627-\u06CC]*[\u0627-\u06CC]", name):	#tak harfi ha ro dast nazan mesle: ب۲
		if len(re.search(r"[\u0627-\u06CC]*[\u0627-\u06CC]", name).group(0))<=1:
			return name
	v_fixed=re.sub(r"([\u06f0-\u06f9]+((\.|\+|\-|\/|\،|\٫)[\u06f0-\u06f9]+)?)",r" \1 ", name).strip()
	v_fixed=re.sub(r"([\u06f0-\u06f9]+([\u06f0-\u06f9]+)?)\b ام\b",r"\1ام", v_fixed).strip()
	# v_fixed=re.sub(r"([ ]?-+(\.-+)?[ ]?)",r" \1 ", v_fixed)
	# v_fixed=v_fixed.replace(" / ","/").replace(" / ","/")
	v_fixed=v_fixed.replace("( ","(").replace(" )",")")
	v_fixed=v_fixed.replace("("," (").replace(")",") ")
	v_fixed=v_fixed.replace("- ","-").replace(" -","-").replace("-  ","-").replace("  -","-").replace(" - ","-").replace("  -  ","-")
	v_fixed=re.sub(r'(\b-\b)', ' - ', v_fixed)
	v_fixed=re.sub(r'(\b -\b)', ' - ', v_fixed)
	v_fixed=re.sub(r'(\b- \b)', ' - ', v_fixed)
	v_fixed=v_fixed.replace("-- ","--").replace(" --","--").replace("--  ","--").replace("  --","--").replace(" -- ","--").replace("  --  ","--")
	v_fixed=re.sub(r'(\b--\b)', ' -- ', v_fixed)
	v_fixed=re.sub(r'(\b --\b)', ' -- ', v_fixed)
	v_fixed=re.sub(r'(\b-- \b)', ' -- ', v_fixed)
	v_fixed=v_fixed.replace("  "," ").replace("  "," ").replace("  "," ")
	v_fixed=v_fixed.strip()
	return v_fixed
	
log("* Loading File")
tree = ET.parse('input.osm')		#Source input file name
root = tree.getroot()
log("* File loaded, Fixing.")


log("Working on Nodes:")
counter = 0
for node in root.findall('node'):
	for tag in node.findall('tag'):
		k = tag.attrib['k']
		if k=='name':
			name = tag.attrib['v']
			v_fixed = dofixes(name)
			print (v_fixed)
			if v_fixed != name:
				counter=counter+1
				tag.attrib['v'] = v_fixed
				node.set('action', 'modify')
				logchanges(counter,"node/"+node.attrib['id'],name,v_fixed)
info = info + "\n" + str(counter) + " Node & "


log("Working on Ways:")
counter = 0
for way in root.findall('way'):
	for tag in way.findall('tag'):
		k = tag.attrib['k']
		if k=='name':
			name = tag.attrib['v']
			v_fixed = dofixes(name)
			print (v_fixed)
			if v_fixed != name:
				counter=counter+1
				tag.attrib['v'] = v_fixed
				way.set('action', 'modify')
				logchanges(counter,"way/"+way.attrib['id'],name,v_fixed)
info = info + str(counter) + " Way Fixed."


# log("Working on Relations:")
# for relation in root.findall('relation'):
	# for tag in relation.findall('tag'):
		# k = tag.attrib['k']
		# if k=='name':
			# name = tag.attrib['v']
			# v_fixed = dofixes(name)
			# print (v_fixed)
			# if v_fixed != name:
				# counter=counter+1
				# tag.attrib['v'] = v_fixed
				# node.set('action', 'modify')
				# logchanges(counter,"node/"+node.attrib['id'],name,v_fixed)
# info = info + "\n" + str(counter) + " Relation Fixed."

log (info)
log ("")
log ("* Writing to output file")
tree.write('output.osm',encoding="UTF-8")
log("------------------------------------------------------------------------------------------")
log ("* Closing files")
logfile.close
logchangesfile.write(b"""</table></body>
</html>""")
logchangesfile.close
info = info.replace("\n","\n<br>")
with open(os.path.dirname(os.path.realpath(__file__))+"/logs/Changes_"+currentdatetime+".html", "rb") as logchangesfile:
	newText=logchangesfile.read().replace(b'{{info}}', info.encode('utf8'))
 
with open(os.path.dirname(os.path.realpath(__file__))+"/logs/Changes_"+currentdatetime+".html", "wb") as logchangesfile:
	logchangesfile.write(newText)
log ("* Done.")

#comment:
'''
[out:xml][timeout:180];
{{geocodeArea:iran}}->.searchArea;
(
  node["name"~"^[\u0627-\u06cc]+[\u06f0-\u06f9].+|[\u06f0-\u06f9]+[\u0627-\u06cc].+$"](area.searchArea);
  way["name"~"^[\u0627-\u06cc]+[\u06f0-\u06f9].+|[\u06f0-\u06f9]+[\u0627-\u06cc].+$"](area.searchArea);
 );
(._;>;);
out meta;
'''

